# README

Assignment for Ruby developer

Build sample web application as below:

* Login page

* Member directory
    * Members directory: all members listed, by default in alphabetical order. Member information is sample data.
    * Search members. Based on the following associated data: Name, Region, Interests, Specialism
    * Pagination

Non-functional requirements:

* Technologies 
   * Backend 
        * Ruby on Rails 5
        * PostgreSQL 
   * Frontend
        * AngularJS 1
        * Grunt 

* Well designed, unit testable

* Unit Test + Automation Test with 70% code coverage 

* Apply automation deployment


# Run project
```
$: bundle install
$: rails db:migrate
$: rails db:seed
$: rails s -b 0.0.0.0 -p 3000
```

# Client management link
```
http://localhost:3000/oauth/applications
```

# API get token:
```
localhost:3000/oauth/token
Data:
client_id: "b394fdac898be03cd0533f583a8b104c8e91f0ceeaa22008df5514d59a7e762b"
client_secret: "3d565a5222509ba1ced6c24461aa174811bb7e207c32a41c86883f8f3d4064a7"
grant_type: "password"
username: "example@example.com",
password: "admin123",
```

