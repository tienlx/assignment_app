class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_filter :verify_authenticity_token

  $default_limit = 5
  $default_offset = 0

  def hello
    render html: "Welcome to My App."
  end

  def doorkeeper_unauthorized_render_options(error: nil)
    { json: { :status => :error, :message => "Not authorized" } }
  end
end
