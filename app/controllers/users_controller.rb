class UsersController < ApplicationController
  before_action :doorkeeper_authorize!



  def new
  end

  def index
    limit = params[:limit]? params[:limit] : $default_limit
    offset = params[:offset] ? params[:offset] : $default_offset
    users = User.all.limit(limit).offset(offset)
    users_count = User.count
    # return_data = users
    return_data = Hash.new(-1)
    return_data['users'] = users
    return_data['total'] = users_count
    render :json => { :status => :ok, :message => "Success!", :data => return_data}
  end

  def show
    user = User.find(params[:id])
    return_data = user
    render :json => { :status => :ok, :message => "Success!", :data => return_data}
  end

  def search
    limit = params[:limit] ? params[:limit] : $default_limit
    offset = params[:offset] ? params[:offset] : $default_offset
    results_count = User.where("name LIKE ?", "%#{params[:search_str]}%").count
    users = User.where("name LIKE ?", "%#{params[:search_str]}%").limit(limit).offset(offset)

    return_data = Hash.new(-1)
    return_data['users'] = users
    return_data['total'] = results_count

    render :json => { :status => :ok, :message => "Success!", :data => return_data}
  end


end
