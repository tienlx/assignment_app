Rails.application.routes.draw do
  use_doorkeeper
  get 'users/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#hello'
  resources :users
  post '/users/search', to: 'users#search'
end
