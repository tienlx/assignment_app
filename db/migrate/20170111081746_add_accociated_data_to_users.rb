class AddAccociatedDataToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :email, :string
    add_column :users, :region, :string
    add_column :users, :interests, :string
    add_column :users, :specialism, :string
  end
end
