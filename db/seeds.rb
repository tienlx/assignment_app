# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name:  "Example User",
             email: "example@example.com",
             region: "South Viet Nam",
             interests: "Coding",
             specialism: "Developer",
             password:              "admin123",
             password_confirmation: "admin123")

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@example.com"
  password = "123456"
  region = Faker::Address.state
  specialism = Faker::Company.profession
  interests = Faker::Lorem.word
  User.create!(name:  name,
               email: email,
               region: region,
               interests: interests,
               specialism: specialism,
               password:              password,
               password_confirmation: password)
end
