require "rails_helper"

# controllers/users_controller.rb
RSpec.describe UsersController, :type => :controller do
  describe 'GET test not authorized' do
    it 'responds with 401' do
      get :index, :format => :json
      response.status.should eq(401)
    end
  end

  describe 'GET #index' do
    let(:token) { double :acceptable? => true }

    before do
      controller.stub(:doorkeeper_token) { token }
      # allow(controller).to receive(:doorkeeper_token) {token} # => RSpec 3
    end

    it 'responds with 200' do
      get :index, :format => :json
      response.status.should eq(200)
    end
  end

  describe 'GET #show' do
    let(:token) { double :acceptable? => true }

    before do
      controller.stub(:doorkeeper_token) { token }
      # allow(controller).to receive(:doorkeeper_token) {token} # => RSpec 3
      test_user = User.create!(name:  "Example User",
             email: "example@example.com",
             region: "South Viet Nam",
             interests: "Coding",
             specialism: "Developer",
             password:              "admin123",
             password_confirmation: "admin123")
      get :show, id: test_user.id, format: :json
     end

    it 'responds with 200' do
      response.status.should eq(200)
    end
  end

  describe 'GET #search' do
    let(:token) { double :acceptable? => true }

    before do
      controller.stub(:doorkeeper_token) { token }
      # allow(controller).to receive(:doorkeeper_token) {token} # => RSpec 3
      test_user = User.create!(name:  "Example User",
             email: "example@example.com",
             region: "South Viet Nam",
             interests: "Coding",
             specialism: "Developer",
             password:              "admin123",
             password_confirmation: "admin123")
      99.times do |n|
        name  = Faker::Name.name
        email = "example-#{n+1}@example.com"
        password = "123456"
        region = Faker::Address.state
        specialism = Faker::Company.profession
        interests = Faker::Lorem.word
        User.create!(name:  name,
                     email: email,
                     region: region,
                     interests: interests,
                     specialism: specialism,
                     password:              password,
                     password_confirmation: password)
      end
      post :search, search_str: 'Example', format: :json
     end

    it 'responds with 200' do
      response.status.should eq(200)
    end
  end
end
