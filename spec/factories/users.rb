FactoryUser.define do
  factory :user do
    name "Example User",
    email "example@example.com",
    region "South Viet Nam",
    interests "Coding",
    specialism"Developer",
    password "admin123",
    password_confirmation "admin123"
  end
end
