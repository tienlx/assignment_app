require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name:  "Example User",
             email: "example@example.com",
             region: "South Viet Nam",
             interests: "Coding",
             specialism: "Developer",
             password:              "admin123",
             password_confirmation: "admin123")
  end

  test "should be valid" do
    assert @user.valid?
  end
end
